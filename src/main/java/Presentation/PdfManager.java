package Presentation;

import Models.Order;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * Class for generating pdf file
 */

public class PdfManager {
    private int clientReportCount = 0;
    private int productReportCount = 0;
    private int orderReportCount = 0;
    private int billCount = 0;
    private int stockWarningCount = 0;
    private int notFoundCount = 0;
    private String path = "C:\\Users\\undea\\Desktop\\pdfs\\";

    /**
     * Generates a pdf if certain object doesn't exist in database
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void notFound() throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(path + "not_found_" + (++notFoundCount) + ".pdf"));
        document.open();

        Chunk chuck = new Chunk("Client or product not found");
        document.add(chuck);
        document.close();
    }

    /**
     * Generates pdf if there is not enough stock of a product
     * @param order
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void notEnoughStock(Order order) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(path + "not_enough_stock_" + (++stockWarningCount) + ".pdf"));
        document.open();

        Chunk chuck = new Chunk("Not enough " + order.getProductName() + "s in stock");
        document.add(chuck);
        document.close();
    }

    /**
     * Generates a pdf/bill with a corresponding order
     * @param order The order
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void placeBill(Order order) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(path + "bill_" + (++billCount) + ".pdf"));
        document.open();

        Chunk bill = new Chunk("consumer's name: " + order.getClientName() + ", product chosen: " + order.getProductName() + ", quantity of product: " + order.getQuantity());
        document.add(bill);

        document.close();
    }

    /**
     * Generates a pdf with the data of a table
     * @param results The data
     * @param name Table name
     * @throws DocumentException
     * @throws FileNotFoundException
     * @throws SQLException
     */
    public void makePdf(ArrayList<String> results, String name) throws DocumentException, FileNotFoundException, SQLException {
        Document document = new Document();
        PdfPTable table = null;

        if (name.equals("client")){
            PdfWriter.getInstance(document, new FileOutputStream(path + "client_report_" + (++clientReportCount) + ".pdf"));
            document.open();
            table = new PdfPTable(2);
        }
        else if (name.equals("product")){
            PdfWriter.getInstance(document, new FileOutputStream(path + "product_report_" + (++productReportCount) + ".pdf"));
            document.open();
            table = new PdfPTable(3);
        }
        else if (name.equals("order")){
            PdfWriter.getInstance(document, new FileOutputStream(path + "order_report_" + (++orderReportCount) + ".pdf"));
            document.open();
            table = new PdfPTable(3);
        }

        addTableHeader(table, name);
        addRows(table, results, name);

        document.add(table);
        document.close();
    }

    /**
     * Adds rows to the pdf table
     * @param table
     * @param results
     * @param name
     * @throws SQLException
     */
    private void addRows(PdfPTable table, ArrayList<String> results, String name) throws SQLException {
            for (String s :
                    results) {
                table.addCell(s);
            }
    }

    /**
     * Adds the headers to the pdf table
     * @param table
     * @param name
     */
    public void addTableHeader(PdfPTable table, String name){
        if (name.equals("client"))
            Stream.of("name", "address")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        else if (name.equals("product"))
            Stream.of("name", "quantity", "price")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        else if (name.equals("order"))
            Stream.of("client name", "product name", "quantity")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
    }
}
