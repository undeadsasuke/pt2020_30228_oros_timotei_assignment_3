package Presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Parses a file of commands
 */

public class ParseFile {
    /**
     * Path to file
     */
    private String path;
    private File file;
    private Scanner scanner;
    /**
     * List that holds commands
     */
    private ArrayList<String> commands;

    public ParseFile(String path) throws FileNotFoundException {
        this.path = path;
        file = new File(path);
        scanner = new Scanner(file);
        commands = new ArrayList<String>();
    }

    /**
     * Parses the file
     * @return The individual commands
     */
    public ArrayList<String> getCommands(){
        while (scanner.hasNextLine()){
            String command = scanner.nextLine();
            commands.add(command);
        }

        return commands;
    }
}
