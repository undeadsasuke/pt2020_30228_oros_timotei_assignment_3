package Main;

import DataAccess.ConnectionFactory;
import Logic.CommandInterpreter;
import Presentation.ParseFile;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * MainClass is where the file is parsed and the commands are sent to be executed
 */

public class MainClass {
    public static void main(String[] args) throws FileNotFoundException, SQLException, DocumentException {
        ParseFile input = new ParseFile(args[0]);
        CommandInterpreter interpreter = new CommandInterpreter(input.getCommands());
        interpreter.sendCommands();
    }
}
