package DataAccess;

import Models.Client;
import Models.Order;
import Models.Product;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Performs database operations
 */

public class DataManipulation {
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    private String reportSql = "SELECT * FROM value;";
    private String insertClientSql = "INSERT INTO client (client_name, address) VALUES (values);";
    private String insertProductSql = "INSERT INTO product (product_name, quantity, price) VALUES (values);";
    private String deleteClientSql = "DELETE FROM client WHERE client_name = 'value1' AND address = 'value2';";
    private String getProductNameSql = "SELECT product_name FROM product WHERE product_name = 'value';";
    private String getQuantitySql = "SELECT quantity FROM product WHERE product_name = 'value';";
    private String updateQuantitySql = "UPDATE product SET quantity = value1 WHERE product_name = 'value2'";
    private String deleteProductSql = "DELETE FROM product WHERE product_name = 'value';";
    private String makeOrderSql = "INSERT INTO `order` (client_name, product_name, quantity) VALUES (values);";
    private String clientExistenceSql = "SELECT * FROM client WHERE client_name = 'value';";
    private String productExistenceSql = "SELECT * FROM product WHERE product_name = 'value';";

    /**
     * Gets the database connection
     */
    public DataManipulation(){
        connection = ConnectionFactory.getConnection();
    }

    /**
     * Inserts a client into the database
     * @param client Client to be inserted
     * @throws SQLException
     */
    public void insert(Client client) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(insertClientSql.replace("values", "'" + client.getName() + "', '" + client.getAddress() + "'"));
        ConnectionFactory.close(statement);
    }

    /**
     * Inserts a product into the database
     * @param product Product to be inserted
     * @throws SQLException
     */
    public void insert(Product product) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(insertProductSql.replace("values", "'" + product.getName() + "', " + product.getQuantity() + ", " + product.getPrice()));
        ConnectionFactory.close(statement);
    }

    /**
     * Inserts an order into the database
     * @param order Order to be inserted
     * @throws SQLException
     */
    public void insert(Order order) throws  SQLException{
        Statement statement = connection.createStatement();
        statement.executeUpdate(makeOrderSql.replace("values", "'" + order.getClientName() + "', '" + order.getProductName() + "', " + order.getQuantity()));
        ConnectionFactory.close(statement);
    }

    /**
     * Deletes a client from the database
     * @param client Client to be deleted
     * @throws SQLException
     */
    public void delete(Client client) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(deleteClientSql.replace("value1", client.getName()).replace("value2", client.getAddress()));
        ConnectionFactory.close(statement);
    }

    /**
     * Deletes a product from the database
     * @param productName Product to be deleted
     * @throws SQLException
     */
    public void delete(String productName) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(deleteProductSql.replace("value", productName));
        ConnectionFactory.close(statement);
    }

    /**
     * Updates product quantity
     * @param product Product to be updated
     * @param quantity Quantity to be added
     * @throws SQLException
     */
    public void update(Product product, int quantity) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(updateQuantitySql.replace("value1", Integer.toString(quantity + product.getQuantity())).replace("value2", product.getName()));
        ConnectionFactory.close(statement);
    }

    /**
     * Updates product quantity
     * @param quantity New quantity
     * @param name Product to be updated
     * @throws SQLException
     */
    public void update(int quantity, String name) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(updateQuantitySql.replace("value1", Integer.toString(quantity)).replace("value2", name));
        ConnectionFactory.close(statement);
    }

    /**
     * Returns a product exists
     * @param name Product name
     * @return Return product name
     * @throws SQLException
     */
    public String getProductName(String name) throws SQLException {
        statement = connection.createStatement();
        resultSet = statement.executeQuery(getProductNameSql.replace("value", name));

        String productName = null;
        if (resultSet.next())
            productName = resultSet.getString("product_name");
        ConnectionFactory.close(statement);
        ConnectionFactory.close(resultSet);

        return productName;
    }

    /**
     * Returns a product quantity
     * @param name Name of the product
     * @return The quantity
     * @throws SQLException
     */
    public int getProductQuantity(String name) throws SQLException {
        statement = connection.createStatement();
        resultSet = statement.executeQuery(getQuantitySql.replace("value", name));

        int quantity = 0;
        if (resultSet.next())
            quantity = resultSet.getInt("quantity");
        ConnectionFactory.close(statement);
        ConnectionFactory.close(resultSet);

        return quantity;
    }

    /**
     * Checks if certain object exists in database
     * @param name Name of the object
     * @param checkWhat Object type
     * @return The object
     * @throws SQLException
     */
    public boolean checkExistence(String name, String checkWhat) throws SQLException {
        statement = connection.createStatement();

        if (checkWhat.equals("client"))
            resultSet = statement.executeQuery(clientExistenceSql.replace("value", name));
        if (checkWhat.equals("product"))
            resultSet = statement.executeQuery(productExistenceSql.replace("value", name));


        if (resultSet.next() == false){
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);
            return false;
        }
        else{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);
            return true;
        }
    }

    /**
     * Deletes information from all tables
     * @throws SQLException
     */
    public void deleteAll() throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate("DELETE FROM client;");
        ConnectionFactory.close(statement);

        statement = connection.createStatement();
        statement.executeUpdate("DELETE FROM `order`;");
        ConnectionFactory.close(statement);

        statement = connection.createStatement();
        statement.executeUpdate("DELETE FROM product;");
        ConnectionFactory.close(statement);
    }

    /**
     * Returns the data from a centain table
     * @param name Table name
     * @return Table data
     * @throws SQLException
     */
    public ArrayList<String> report(String name) throws SQLException {
        Statement statement = connection.createStatement();
        resultSet = statement.executeQuery(reportSql.replace("value", name));

        ArrayList<String> results = new ArrayList<String>();

        if (name.equals("client"))
            while (resultSet.next()) {
                results.add(resultSet.getString("client_name"));
                results.add(resultSet.getString("address"));
            }

        if (name.equals("product"))
            while (resultSet.next()) {
                results.add(resultSet.getString("product_name"));
                results.add(resultSet.getString("quantity"));
                results.add(resultSet.getString("price"));
            }

        if (name.equals("`order`"))
            while (resultSet.next()) {
                results.add(resultSet.getString("client_name"));
                results.add(resultSet.getString("product_name"));
                results.add(resultSet.getString("quantity"));
            }
        ConnectionFactory.close(statement);
        ConnectionFactory.close(resultSet);
        return results;
    }
}
