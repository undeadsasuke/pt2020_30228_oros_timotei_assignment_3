package DataAccess;

import java.sql.*;

/**
 * Creates connection to the database using singleton design pattern
 */

public class ConnectionFactory {
    /**
     * Database driver
     */
    public static final String DRIVER = "com.mysql.jdbc.Driver";
    /**
     * Database url
     */
    public static final String URL = "jdbc:mysql://localhost:3306/assignment3";
    /**
     * Database user
     */
    public static final String USER = "root";
    /**
     * Database password
     */
    public static final String PASSWORD = "root";

    /**
     * ConnectionFactory instance
     */
    private static ConnectionFactory singleInstance = new ConnectionFactory();

    /**
     * Gets driver
     */
    private ConnectionFactory(){
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    /**
     * Creates a database connection with the corresponding fields
     * @return The connection
     */
    private Connection createConnection(){
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e){
            System.out.println("Unable to connect to database");
        }
        return connection;
    }

    /**
     * Returns the connection
     * @return The connection
     */
    public static Connection getConnection(){
        return singleInstance.createConnection();
    }

    /**
     * Closes the database connection
     * @param connection The database connection to be closed
     */
    public static void close(Connection connection){
        if (connection != null){
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("Could not close connection");
            }
        }
    }

    /**
     * Closes the statement
     * @param statement The statement to be closed
     */
    public static void close(Statement statement){
        if (statement != null){
            try {
                statement.close();
            } catch (SQLException e) {
                System.out.println("Could not close statement");
            }
        }
    }

    /**
     * Closes the result set
     * @param resultSet The result set to be closed
     */
    public static void close(ResultSet resultSet){
        if (resultSet != null){
            try {
                resultSet.close();
            } catch (SQLException e) {
                System.out.println("Could not close result set");
            }
        }
    }

}
