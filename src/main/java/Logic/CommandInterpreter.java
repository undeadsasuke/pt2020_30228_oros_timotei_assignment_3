package Logic;

import Models.Client;
import Models.Order;
import Models.Product;
import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * CommandInterpreter takes the commands from the file and sends them to the corresponding executable method
 */

public class CommandInterpreter {
    /**
     * The list of commands
     */
    private ArrayList<String> commands;
    /**
     * The object whom will pass the commands
     */
    private ExecuteCommands execute;

    /**
     * Gets the commands and instantiates execute object
     * @param commands The commands from ParseFile
     * @throws SQLException
     */
    public CommandInterpreter(ArrayList<String> commands) throws SQLException {
        this.commands = commands;
        execute = new ExecuteCommands();
    }

    /**
     * <p> Sends the commands from the file to the method that executes the command</p>
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void sendCommands() throws SQLException, FileNotFoundException, DocumentException {
        for (String command :
                commands) {
            if (command.contains("Insert client:")){
                Client client = (Client) splitIntoData("client", command.substring(15));
                execute.insertClient(client);
            } else if (command.contains("Insert product:")){
                Product product = (Product) splitIntoData("product", command.substring(16));
                execute.insertProduct(product);
            } else if (command.contains("Order:")){
                Order order = (Order) splitIntoData("order", command.substring(7));
                execute.makeOrder(order);
            } else if (command.contains("Delete client:")){
                Client client = (Client) splitIntoData("dclient", command.substring(15));
                execute.deleteClient(client);
            } else if (command.contains("Delete Product:")){
                execute.deleteProduct(command.substring(16));
            } else if (command.contains("Report client")){
                execute.reportClient();
            } else if (command.contains("Report product")){
                execute.reportProduct();
            } else if (command.contains("Report order")){
                execute.reportOrder();
            }
        }
    }

    /**
     * <p>Splits the sting with the regex "\\,"</p>
     * @param splitFor Every string has to be split accordingly, for it's command
     * @param string The string to be split
     * @return Returns nothing
     */
    private Object splitIntoData(String splitFor, String string){
        if(splitFor.equals("client")){
            String[] parts = string.split("\\,");
            return new Client(parts[0], parts[1].substring(1));
        }

        if(splitFor.equals("product")){
            String[] parts = string.split("\\,");
            return new Product(parts[0], Integer.parseInt(parts[1].substring(1)), Float.parseFloat(parts[2].substring(1)));
        }

        if(splitFor.equals("order")){
            String[] parts = string.split("\\,");
            return new Order(parts[0], parts[1].substring(1), Integer.parseInt(parts[2].substring(1)));
        }

        if(splitFor.equals("dclient")){
            String[] parts = string.split("\\,");
            return new Client(parts[0], parts[1].substring(1));
        }

        return null;
    }
}
