package Logic;

import DataAccess.ConnectionFactory;
import DataAccess.DataManipulation;
import Models.Client;
import Models.Order;
import Models.Product;
import Presentation.PdfManager;
import com.itextpdf.text.DocumentException;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import javax.xml.crypto.Data;
import java.io.FileNotFoundException;
import java.sql.*;

/**
 * ExecuteCommands take the commands from CommandInterpreter and executes the necessary methods for the command
 */

public class ExecuteCommands {
    /**
     * The commands will have to access a database
     */
    private DataManipulation database;
    /**
     * Some commands will have to generate pdf
     */
    private PdfManager pdfManager;

    /**
     *
     * Instantiate fields and execute a delete on the database, so the tables will be empty on each execution of the program
     * @throws SQLException
     */
    public ExecuteCommands() throws SQLException {
        database = new DataManipulation();
        database.deleteAll();
        pdfManager = new PdfManager();
    }

    /**
     * Sends the client to the database for insertion
     * @param client Client to be inserted
     * @throws SQLException
     */
    public void insertClient(Client client) throws SQLException {
        database.insert(client);
    }

    /**
     * Sends the product to the database for insertion
     * @param product Product to be inserted
     * @throws SQLException
     */
    public void insertProduct(Product product) throws SQLException {
        String productName = database.getProductName(product.getName());

        if(product.getName().equals(productName)){
            int quantity = database.getProductQuantity(product.getName());
            database.update(product, quantity);
        } else{
            database.insert(product);
        }
    }

    /**
     * Sends order to the database for insertion
     * @param order Order to be inserted
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void makeOrder(Order order) throws SQLException, FileNotFoundException, DocumentException {
        int quantity = database.getProductQuantity(order.getProductName());
        quantity -= order.getQuantity();

        if (database.checkExistence(order.getClientName(), "client") == false || database.checkExistence(order.getProductName(), "product") == false){
            pdfManager.notFound();
            return;
        }
            if(quantity >= 0){
                database.insert(order);
                database.update(quantity, order.getProductName());
                pdfManager.placeBill(order);
            } else
                pdfManager.notEnoughStock(order);
    }

    /**
     * Sends client to the database for deletion
     * @param client Client to be deleted
     * @throws SQLException
     */
    public void deleteClient(Client client) throws SQLException {
        database.delete(client);
    }

    /**
     * Sends product to the database for deletion
     * @param name Product to be deleted
     * @throws SQLException
     */
    public void deleteProduct(String name) throws SQLException {
        database.delete(name);
    }

    /**
     * Tells PdfManager to make a pdf for the client table
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void reportClient() throws SQLException, FileNotFoundException, DocumentException {
        pdfManager.makePdf(database.report("client"), "client");
    }

    /**
     * Tells PdfManager to make pdf for the product table
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void reportProduct() throws SQLException, FileNotFoundException, DocumentException {
        pdfManager.makePdf(database.report("product"), "product");
    }

    /**
     * Tells PdfManager to make pdf for the order table
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void reportOrder() throws SQLException, FileNotFoundException, DocumentException {
        pdfManager.makePdf(database.report("`order`"), "order");
    }
}
