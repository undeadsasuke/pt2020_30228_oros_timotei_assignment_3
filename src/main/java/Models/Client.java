package Models;

/**
 * Model class for representing a client
 */

public class Client {
    /**
     * Client's name
     */
    private String name;
    /**
     * Client's address
     */
    private String address;

    public Client(String name, String address){
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
