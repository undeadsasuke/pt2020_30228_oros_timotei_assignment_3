package Models;

/**
 * Model class representing a product
 */

public class Product {
    /**
     * Product's name
     */
    private String name;
    /**
     * Product's quantity
     */
    private int quantity;
    /**
     * Products price
     */
    private float price;

    public Product(String name, int quantity, float price){
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
