package Models;

/**
 * Model class representing an order
 */

public class Order {
    /**
     * Order's client name
     */
    private String clientName;
    /**
     * Orders product name
     */
    private String productName;
    /**
     * Orders product quantity
     */
    private int quantity;

    public  Order(String clientName, String productName, int quantity){
        this.clientName = clientName;
        this.productName = productName;
        this.quantity = quantity;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
